.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
USB
---

**Discussion**:

The AVPS working group and industry consensus seems to be that it is
difficult to give concurrent access to USB hardware from more than one
operating system instance, but we elaborate on the possibilities and
needs in this chapter. It turns out that some research and
implementation has been done in this area, but at this point it is not
certain how it would affect a standard platform definition. In any case,
the discussion section provides a lot of thinking about both needs and
challenges.

.. note :: [VIRTIO] does not in its current version mention USB.

The USB protocol has explicit host (master) and device (slave) roles,
and communication is peer-to-peer only and never one-to-many. We must
therefore always be clear on which role we are discussing when speaking
about a potential virtual device:

:underline:`Virtualizing USB Host Role`

Concurrent access to a host controller would require creating multiple
virtual USB devices (here in the meaning of virtual hardware device, not
the USB device role), that are mapped onto a single hardware implemented
host role, which i.e. a single USB host port. To make sharing
interesting we first assume that a USB Hub is connected so that multiple
devices can be attached to this shared host. Presumably,
partitioning/filtering of the tree of attached devices could be done so
that different virtual hosts are seeing only a subset of the devices.
The host/device design of the USB protocol makes it very challenging to
have more than one software stack playing the host role. When devices
connect, there is an enumeration and identification procedure
implemented in the host software stack. This procedure cannot have
multiple masters. At this time, considering how USB host can be
virtualized is an interesting theoretical exercise but value trade-off
does not seem to be there, despite some potential ways it might be used
if it were possible (see use-case section). We don’t rule out the
possibility of research into this changing the perception, however.

:underline:`Virtualizing USB Devices`

This could possibly mean two things. First, consider a piece of hardware
that implements the USB-device role, and that hardware runs multiple
VMs. Such virtualization seems next to nonsensical. A USB-device tend to
be a very dedicated hardware device with a single purpose (yes,
potentially more than one role is possible, but they tend to be
related). Implementing the function of the USB-device would be best
served by one system (a single Virtual Machine in a consolidated
system). Thus, at most it seems that pass-through is the realistic
solution.

The second interpretation is the idea of allowing multiple (USB host
role) VMs to concurrently use a single actual USB-device hardware. This
is difficult due to the single-master needs for enumerating and
identifying that device. It is rather the higher-level function of the
device (e.g. file storage, networking, etc.) that may need to be shared
but not the low-level hardware interaction. Presumably, therefore a
single VMs must in practice reserve the USB device hardware during its
use and no concurrency is expected to be supported. Also here, research
may show interesting results, but we saw little need to delve into it at
this time.

:underline:`Use cases and solutions`

There are cases to be made for more than one VM needing access to a
single USB device. For example, a single mass-storage device (USB
memory) may be required to provide files to more than one subsystem
(VM). There are many potential use cases but just as an example,
consider software/data update files that need to be applied to more than
one VM/guest, or media files being played by one guest system whereas
navigation data is needed in another.

During the writing of this specification we found that some research had
into USB virtualization but there was not time to move that into a
standard.

After deliberation we have decided for the moment to assume that
hypervisors will provide only pass-through access to USB hardware (both
host and device roles)

USB On-The-Go(tm) is also left out of scope, since most automotive
systems implement the USB host role only, and in the case a system ever
needs to have the device role it would surely have a dedicated port and
a single operating system instance handling it.

A general discussion for any function in the virtual platform is whether
pass-through and dedicated access is to be fixed (at system
definition/compile time, or at boot time), or possible to request
through an API during runtime.

The ability for one VM to request dedicated access to the USB device
during runtime is a potential improvement and it ought to be
considered when choosing a hypervisor. With such a feature, VMs could
even alternate their access to the USB port with a simple
acquire/release protocol. It should be noted of course that it raises
many considerations about reliability and one system starving the
other of
access. Such a solution would only apply if policies, security and
other considerations are met for the system.

The most likely remaining solution to our example of exposing different
parts of a file collection to multiple VMs is then that one VM is
assigned to be the USB master and provide access to the filesystem (or
part of it) by means of VM-to-VM communication. For example, a network
file system such as NFS or any equivalent solution could be used.

:underline:`Special hardware support for virtualization`

As noted, it seems likely implementing protocols to split a single host
port between multiple guests is complicated. This applies also if the
hardware implements not only host controllers but also a USB-hub. In
other words, when considering the design of SoCs to promote or support
USB virtualization, it seems a more straightforward solution to simply
provide more separate USB hardware devices on the SoC (that can be
assigned to VMs using pass-through), than to build in special
virtualization features into the hardware.

That does not solve the use case of concurrent access to a device but as
we could see there are likely software solutions that are better.

**AVPS Requirements**:

The following requirements are limited and expected to be increased in
the future, due to the challenges mentioned in the Discussion section
and that more investigation of already performed work (research papers,
etc.) needs to be done.


**Configurable pass-through access to USB devices.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-33}
    - The hypervisor MUST provide statically configurable
      pass-through access to each USB host controller.

**Resource API for USB devices**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-34}
    - The hypervisor MAY optionally provide an API/protocol
      to request USB access from the virtual machine, during normal runtime.


.. warning:: The configuration of pass-through for USB is yet not
  standardized and for the moment considered a proprietary API. This is a
  potential for future improvement.

.. todo :: Potential for future work exists here.
