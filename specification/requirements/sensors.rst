.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Sensors
-------

**Discussion:**

Most of what are considered sensors in a car are deeply integrated with
electronics or associated with dedicated ECUs and accessing their data
may already be defined by the protocols that the ECUs or electronics
provide and as such the protocol is unrelated to any virtual platform
standardization.

However, as SoCs become more integrated there are often a variety of
sensors implemented on the same silicon and directly addressable. As
such they may be candidates for a device sharing setup. Sensors such as
ambient light, temperature, pressure, acceleration, IMU Inertial
Measurement Unit (rotation), tend to be built into SoCs because they
share similarities with mobile phone SoCs that require these.

The Systems Control Management Interface (SCMI) specification, ref:
[SCMI], defines a kind of protocol to access peripheral hardware. It is
usually spoken from general CPU cores to the system controller (M3 core
responsible for clock tree, power regulation, etc.) via a hardware
mailbox.

Since this protocol is already defined and suitable for communication
between, it would be possible to reuse it for accessing sensor data
quite independently of where the sensor is located.

The Systems Control Management Interface (SCMI) specification does not
specify the transport, suggesting hardware mailboxes but acknowledging
that this can be different.

Access to the actual sensor hardware can be handled by a dedicated
co-processor or the hypervisor implementation and provide the sensor
data through a communication protocol.

For sensors that are not appropriate to virtualize we instead consider
hardware pass-through.

The SCMI specified protocol was not originally defined for the
virtual-sensor purpose but describes a flexible and an appropriate
abstraction for sensors. It is also appropriate for controlling
power-management and related things. The actual hardware access
implementation is according to ARM
offloaded to a "Systems Control Processor”, but this is an abstract
definition. It could be a dedicated core in some cases and in others
not.

An IIO (Industrial I/O subsystem) driver has been merged into the Linux
Kernel (5.13) and this is based on SCMI (version 3). [SCMI-IIO]

The 3.0 version of SCMI requires timestamps which we think is critical
and therefore SCMI 3.0 is required here for sensor data, instead of SCMI
2.0.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-v2.0-39}
    - For sensors that need to be virtualized the SCMI
      protocol MUST be used to expose sensor data from a sensor subsystem
      to the virtual machines.

      Required SCMI version for sensor interfaces: 3.0 [SCMI3]
